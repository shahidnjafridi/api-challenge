<?php

namespace App\Repository;

use App\Entity\interest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method interest|null find($id, $lockMode = null, $lockVersion = null)
 * @method interest|null findOneBy(array $criteria, array $orderBy = null)
 * @method interest[]    findAll()
 * @method interest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InterestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, interest::class);
    }

    public function save(interest $interest): void
    {
        $this->_em->persist($interest);
        $this->_em->flush();
    }

    public function update(interest $interest): void
    {
        $this->_em->merge($interest);
        $this->_em->flush();
    }
}
