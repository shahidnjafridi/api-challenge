<?php

namespace App\Controller;

use App\Entity\Interest;
use App\Repository\InterestRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class InterestController extends BaseAPIController
{
    private InterestRepository $interestRepository;

    private SerializerInterface $serializer;

    public function __construct(
        InterestRepository $interestRepository,
        SerializerInterface $serializer,
    ) {
        $this->interestRepository = $interestRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/interests/{id}",
     *     methods={"GET"}
     * )
     */
    public function getAction(string $id): Response
    {
        $entity = $this->interestRepository->findOrFailById($id);

        if (empty($entity)) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(
            $this->serializer->serialize($entity, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/interests/{id}",
     *     methods={"POST"}
     * )
     */
    public function postAction(
        Request $request,
        ValidatorInterface $validator
    ): Response {
        $interest = $this->serializer->deserialize($request->getContent(), Interest::class, 'json');
        $constraintViolations = $validator->validate($interest);

        if (count($constraintViolations) > 0) {
            return $this->createValidationFailureResponse($constraintViolations);
        }

        $this->interestRepository->save($interest);

        return new JsonResponse(
            null,
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/interests/{id}",
     *     methods={"PUT"}
     * )
     */
    public function putAction(
        string $id,
        Request $request,
        ValidatorInterface $validator
    ): Response {
        $interest = $this->serializer->deserialize($request->getContent(), Interest::class, 'json');
        $interest->setId($id);

        $constraintViolations = $validator->validate($interest);

        if (count($constraintViolations) > 0) {
            return $this->createValidationFailureResponse($constraintViolations);
        }

        $this->interestRepository->update($interest);

        return new JsonResponse(
            null,
            Response::HTTP_OK,
            [],
            true
        );
    }
}
