<?php

namespace App\Controller;

use App\Entity\Gift;
use App\Repository\CategoryRepository;
use App\Repository\GiftRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GiftController extends BaseAPIController
{
    private GiftRepository $giftRepository;

    private SerializerInterface $serializer;

    public function __construct(
        GiftRepository $giftRepository,
        SerializerInterface $serializer,
    ) {
        $this->giftRepository = $giftRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/gifts/{id}",
     *     methods={"GET"}
     * )
     */
    public function getAction(string $id): Response
    {
        $entity = $this->giftRepository->findOrFailById($id);

        if (empty($entity)) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(
            $this->serializer->serialize($entity, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/gifts/{id}",
     *     methods={"POST"}
     * )
     */
    public function postAction(
        Request $request,
        ValidatorInterface $validator
    ): Response {
        $gift = $this->serializer->deserialize($request->getContent(), Gift::class, 'json');
        $constraintViolations = $validator->validate($gift);

        if (count($constraintViolations) > 0) {
            return $this->createValidationFailureResponse($constraintViolations);
        }

        $this->giftRepository->save($gift);

        return new JsonResponse(
            null,
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/gifts/{id}",
     *     methods={"PUT"}
     * )
     */
    public function putAction(
        string $id,
        Request $request,
        ValidatorInterface $validator
    ): Response {
        $gift = $this->serializer->deserialize($request->getContent(), Gift::class, 'json');
        $gift->setId($id);

        $constraintViolations = $validator->validate($gift);

        if (count($constraintViolations) > 0) {
            return $this->createValidationFailureResponse($constraintViolations);
        }

        $this->giftRepository->update($gift);

        return new JsonResponse(
            null,
            Response::HTTP_OK,
            [],
            true
        );
    }
}
