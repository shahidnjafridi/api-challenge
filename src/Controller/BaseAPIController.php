<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;

abstract class BaseAPIController
{
    protected function createValidationFailureResponse(ConstraintViolationListInterface $constraintViolations): JsonResponse
    {
        return new JsonResponse(
            $this->createErrorListForResponse($constraintViolations),
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }

    private function createErrorListForResponse(ConstraintViolationListInterface $violationList): array
    {
        $errors = [];

        /** @var ConstraintViolationInterface $violation */
        foreach ($violationList as $violation) {
            $errors[] = [
                'message' => $violation->getMessage(),
                'property' => $violation->getPropertyPath(),
                'value' => $violation->getInvalidValue(),
            ];

            return $errors;
        }
    }
}
