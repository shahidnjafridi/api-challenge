<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CategoryController extends BaseAPIController
{
    private CategoryRepository $categoryRepository;

    private SerializerInterface $serializer;

    public function __construct(
        CategoryRepository $categoryRepository,
        SerializerInterface $serializer,
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/categories/{id}",
     *     methods={"GET"}
     * )
     */
    public function getAction(string $id): Response
    {
        $entity = $this->categoryRepository->findOrFailById($id);

        if (empty($entity)) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(
            $this->serializer->serialize($entity, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/categories/{id}",
     *     methods={"POST"}
     * )
     */
    public function postAction(
        Request $request,
        ValidatorInterface $validator
    ): Response {
        $category = $this->serializer->deserialize($request->getContent(), Category::class, 'json');
        $constraintViolations = $validator->validate($category);

        if (count($constraintViolations) > 0) {
            return $this->createValidationFailureResponse($constraintViolations);
        }

        $this->categoryRepository->save($category);

        return new JsonResponse(
            null,
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/categories/{id}",
     *     methods={"PUT"}
     * )
     */
    public function putAction(
        string $id,
        Request $request,
        ValidatorInterface $validator
    ): Response {
        $category = $this->serializer->deserialize($request->getContent(), Category::class, 'json');
        $category->setId($id);

        $constraintViolations = $validator->validate($category);

        if (count($constraintViolations) > 0) {
            return $this->createValidationFailureResponse($constraintViolations);
        }

        $this->categoryRepository->update($category);

        return new JsonResponse(
            null,
            Response::HTTP_OK,
            [],
            true
        );
    }
}
