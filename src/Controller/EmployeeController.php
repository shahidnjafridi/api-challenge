<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Repository\EmployeeRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class EmployeeController extends BaseAPIController
{
    private EmployeeRepository $employeeRepository;

    private SerializerInterface $serializer;

    public function __construct(
        EmployeeRepository $employeeRepository,
        SerializerInterface $serializer,
    ) {
        $this->employeeRepository = $employeeRepository;
        $this->serializer = $serializer;
    }

    /**
     * @Route(
     *     "/employees/{id}",
     *     methods={"GET"}
     * )
     */
    public function getAction(string $id): Response
    {
        $entity = $this->employeeRepository->findOrFailById($id);

        if (empty($entity)) {
            throw new NotFoundHttpException();
        }

        return new JsonResponse(
            $this->serializer->serialize($entity, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/employees/{id}",
     *     methods={"POST"}
     * )
     */
    public function postAction(
        Request $request,
        ValidatorInterface $validator
    ): Response {
        $employee = $this->serializer->deserialize($request->getContent(), Employee::class, 'json');
        $constraintViolations = $validator->validate($employee);

        if (count($constraintViolations) > 0) {
            return $this->createValidationFailureResponse($constraintViolations);
        }

        $this->employeeRepository->save($employee);

        return new JsonResponse(
            null,
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * @Route(
     *     "/employees/{id}",
     *     methods={"PUT"}
     * )
     */
    public function putAction(
        string $id,
        Request $request,
        ValidatorInterface $validator
    ): Response {
        $employee = $this->serializer->deserialize($request->getContent(), Employee::class, 'json');
        $employee->setId($id);

        $constraintViolations = $validator->validate($employee);

        if (count($constraintViolations) > 0) {
            return $this->createValidationFailureResponse($constraintViolations);
        }

        $this->employeeRepository->update($employee);

        return new JsonResponse(
            null,
            Response::HTTP_OK,
            [],
            true
        );
    }
}
